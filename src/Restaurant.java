package src;

import java.util.ArrayList;

public class Restaurant extends Account {
	
	public Restaurant(String username, String password) {
		this.username = username;
		this.password = password;
		this.fridges = new ArrayList<Refrigerator>();
	}
	
	void addRefrigerator() {
		fridges.add(new Refrigerator(username));
	}
	
	boolean removeRefrigerator(int index) {
		if(fridges.size()<=1) {
			System.out.println("not enough fridges to remove, need 2 or more.");
			return false;
		}
		if(index<0 || index>fridges.size()-1) {
			System.out.println("invalid index.");
			return false;
		}
		else {
			Refrigerator removed = fridges.remove(index);
			//look through all the fridges to find
			//spots to reinsert food from fridge
			//removed.
			int length = removed.food.size();
			if(length==0)
				System.out.println("Empty fridge removed, no Food lost.");
			else {
				for(int i = 0; i<fridges.size(); i++) {
					for(int j = fridges.get(i).food.size()-1; j<5; j++) {
							fridges.get(i).food.add(removed.food.remove(0));
							length--;
							if(length==0)
								break;
					}
					if(length==0)
						break;
				}
				if(length==0) {
					System.out.println("Fridge was removed and all Food inside was moved to other fridges");
				}
				else {
					System.out.println("Fridge was removed but these items were thrown away:");
					for(int i = length-1; i>=0; i--) {
						System.out.println(removed.food.get(i));
					}
				}
			}
			return true;
		}
	}
	
	boolean depositFood(int refrigerator, String food) {
		if(fridges.get(refrigerator).food.size() < 5) {
			fridges.get(refrigerator).food.add(food);
			System.out.println(food + " successfully added to fridge" + refrigerator);
			return true;
		}
		else {
			System.out.println("selected fridge is full");
			return false;
		}
		
	}
	
	String withdrawFood(int refrigerator, String food) {
		String found = "nothing";
		for(int i = 0; i< fridges.get(refrigerator).food.size(); i++) {
			if(fridges.get(refrigerator).food.get(i).equals(food))
				found = fridges.get(refrigerator).food.remove(i);
		}
		return found;
	}
	
	boolean transferFood(int from, int to, String food) {
		String original = withdrawFood(from, food);
		if(original.equals("nothing")) {
			System.out.println(food + " not found in fridge " + from);
			return false;
		}
		if(depositFood(to, food))
			return true;
		else {
			System.out.println("returning " + food + " to original fridge" + from);
			depositFood(from, food);
			return false;
		}
	}
	
	void assignInspector(int refrigerator, String inspector) {
		fridges.get(refrigerator).inspectors.add(inspector);
		System.out.println(inspector + " assigned to fridge" + refrigerator);
	}
	
	void printFridges() {
		System.out.println(username);
		for(int i = 0; i< fridges.size(); i++) {
			System.out.print("Fridge " + i + ": ");
			fridges.get(i).printFood();
			System.out.println();
		}
	}
	
}
