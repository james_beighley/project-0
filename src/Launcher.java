package src;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Launcher {
	
	static Scanner userinput = new Scanner(System.in);
	static File loginfile = new File("src/LoginInfo.txt");
	

	public static void main(String[] args) {
		System.out.println("Welcome to Fridge Management inc.");
		System.out.println("enter '1' if you are a restaurant, ");
		System.out.println("'2' if you are a health inspector,");
		System.out.println("anything else to quit.");
		String choice = userinput.nextLine();
		
		if(choice.equals("2")) {
			InspectorMenu.InspectorStart();
		}
		else if(choice.equals("1")) {
			System.out.println("enter '1' to login, '2' to make a new account, anything else to quit");
			choice = userinput.nextLine();
			if(choice.equals("1")) {
				login();
			}
			else if(choice.equals("2")) {
				signup();
			}
			else {
				
			}
				
		}
		else {
			
		}
		System.out.println("exiting program");

	}
	
	private static void login() {
		boolean notfound = true;
		System.out.println("Please enter your username:");
		String username = userinput.next();
		userinput.nextLine();
		System.out.println("Please enter your password:");
		String password = userinput.next();
		userinput.nextLine();
		try {
			Scanner scanloginfile = new Scanner(loginfile);
			while(scanloginfile.hasNextLine()) {
				String data = scanloginfile.nextLine();
				String[] separate = data.split(":");
				if(separate[0].equals(username) && separate[1].equals(password)) {
					System.out.println("Logging in...");
					insideProgram(username, password);
					notfound = false;
					break;
				}
			}
			if(notfound)
				System.out.println("cannot find a username/password combo that matches, run program again but sign up instead");
			scanloginfile.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
	}
	
	private static void signup() {
		System.out.println("Please enter a new username(no ':' or '-'):");
		String username = userinput.next();
		userinput.nextLine();
		System.out.println("Please enter a new password:");
		String password = userinput.next();
		userinput.nextLine();
		try {
			FileWriter myWriter = new FileWriter(loginfile, true);//boolean needed to ensure append to file rather than overwrite
			myWriter.append(username +":" + password + "\n");
			System.out.println("Successfully registered.");
			System.out.println("Logging in...");
			myWriter.close();
			insideProgram(username, password);
			
		} catch (IOException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
	}
	
	private static void insideProgram(String username, String password) {
		Restaurant myrestaurant = new Restaurant(username, password);
		ArrayList<Refrigerator> allfridges = fetchFridges();
		for(int i = 0; i< allfridges.size(); i++) {
				if(allfridges.get(i).restaurant.equals(username)) {
					myrestaurant.fridges.add(allfridges.remove(i));
					i--;
				}
		}
		
		if(myrestaurant.fridges.size()==0) {
			System.out.println("You do not have any fridges, please add some!");
		}
		boolean stay = true;
		while(stay) {
			myrestaurant.printFridges();
			System.out.println("enter the (key) to invoke the command");
			System.out.println("(1) add refrigerator");
			System.out.println("(2) remove refrigerator");
			System.out.println("(3) deposit food to fridge");
			System.out.println("(4) withdraw food from fridge");
			System.out.println("(5) transfer food from one fridge to another");
			System.out.println("(6) assign inspector to fridge");
			System.out.println("(any other key) save and quit");
			String input = userinput.nextLine();
			switch(input) {
				case "1":
					System.out.println("Adding refrigerator");
					myrestaurant.addRefrigerator();
					break;
				case "2":
					System.out.println("Select the fridge to remove (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetoremove = userinput.nextInt();
					userinput.nextLine();
					if(myrestaurant.removeRefrigerator(fridgetoremove)) 
						System.out.println("Success!");
					else
						System.out.println("Failed to remove refrigerator");
					break;
				case "3":
					System.out.println("Select a fridge to deposit food into (0 to " + (myrestaurant.fridges.size()-1) + "):\"");
					int fridgetodeposit = userinput.nextInt();
					userinput.nextLine();
					System.out.println("Name a food to put inside of the fridge");
					String foodtodeposit = userinput.nextLine();
					//userinput.nextLine();
					myrestaurant.depositFood(fridgetodeposit, foodtodeposit);
					break;
				case "4":
					System.out.println("Select a fridge to withdraw food from (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetowithdraw = userinput.nextInt();
					userinput.nextLine();
					myrestaurant.fridges.get(fridgetowithdraw).printFood();
					System.out.println("Type one of the above options to withdraw");
					String foodtowithdraw = userinput.nextLine();
					String withdrawnItem = myrestaurant.withdrawFood(fridgetowithdraw, foodtowithdraw);
					System.out.println("removed" + withdrawnItem + "from fridge " + fridgetowithdraw);
					break;
				case "5":
					System.out.println("Select a fridge to transfer food from (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetotransferfrom = userinput.nextInt();
					userinput.nextLine();
					myrestaurant.fridges.get(fridgetotransferfrom).printFood();
					System.out.println();
					System.out.println("Type one of the above options to withdraw (the word not the number)");
					String foodtotransfer = userinput.nextLine();
					System.out.println("Select a fridge to transfer food into (0 to " + (myrestaurant.fridges.size()-1) + "):\"");
					int fridgetotransferto = userinput.nextInt();
					userinput.nextLine();
					if(fridgetotransferfrom==fridgetotransferto){
						System.out.println("error: the two fridges cannot be the same");
						break;
					}
					myrestaurant.transferFood(fridgetotransferfrom, fridgetotransferto, foodtotransfer);
					break;
				case "6":
					System.out.println("Select a fridge to assign an inspector to (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetoassigninspector = userinput.nextInt();
					userinput.nextLine();
					System.out.println("select an inspector from the list to assign to fridge" + fridgetoassigninspector);
					File inspectors = new File("src/InspectorLoginInfo.txt");
					try {
						Scanner scan = new Scanner(inspectors);
						while(scan.hasNextLine()) {
							String data = scan.nextLine();
							String[] separate = data.split(":");
							System.out.println(separate[0]);
						}
						scan.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String inspector = userinput.nextLine();
					myrestaurant.assignInspector(fridgetoassigninspector, inspector);
					break;
				default:
					stay = false;
					for(int i = 0; i< myrestaurant.fridges.size();i++)
						allfridges.add(myrestaurant.fridges.get(i));
					saveChanges(allfridges);
					System.out.println("signed out");
			}
		}
		
	}
	
	//stores the fridges in the applications data txt file
	//format:     name:food:inspectors
	
	private static void saveChanges(ArrayList<Refrigerator> allfridges) {
		try {
			FileWriter myWriter = new FileWriter("FridgeData.txt");
			for(int i = 0; i< allfridges.size(); i++) {
				StringBuilder s = new StringBuilder();
				s.append(allfridges.get(i).restaurant);
				s.append(":");
				for(int j = 0; j< allfridges.get(i).food.size(); j++) {
					s.append(allfridges.get(i).food.get(j));
					if(j != allfridges.get(i).food.size()-1)
						s.append("-");
				}
				s.append(":");
				for(int j = 0; j< allfridges.get(i).inspectors.size(); j++) {
					s.append(allfridges.get(i).inspectors.get(j));
					if(j != allfridges.get(i).inspectors.size()-1)
						s.append("-");
				}
				myWriter.append(s.toString() + "\n");
			}
			
			myWriter.close();
			
		} catch (IOException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
	}
	
	
	//loads all fridges stored in the system.
	//format:     name:food:inspectors
	
	private static ArrayList<Refrigerator> fetchFridges(){
		ArrayList<Refrigerator> fridges = new ArrayList<Refrigerator>();
		int currentFridge = 0;
		try {
			//System.out.println("inside try block");
			File userfile = new File("FridgeData.txt");
			Scanner scanuserfile = new Scanner(userfile);
			while(scanuserfile.hasNextLine()) {
				//System.out.println("scanline");
				String data = scanuserfile.nextLine();
				//System.out.println(data);
				String[] separate = data.split(":");
				String name = separate[0];
				fridges.add(new Refrigerator(name));
				if(separate.length>1) {
					String[] foodinfridge = separate[1].split("-");
					for(int j = 0; j< foodinfridge.length; j++){
						fridges.get(currentFridge).food.add(foodinfridge[j]);	
					}
				}
				if(separate.length>2) {
					String[] inspectorsforfridge = separate[2].split("-");
					
					for(int j = 0; j< inspectorsforfridge.length; j++){
						fridges.get(currentFridge).inspectors.add(inspectorsforfridge[j]);	
					}
				}
				
				currentFridge++;
			}
			scanuserfile.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
		
		return fridges;
		
		
	}

	
	
	
	
	
	
	
}
