package src;

import java.util.ArrayList;

public class HealthInspector extends Account {
	public HealthInspector(String username) {
		this.username = username;
		fridges = new ArrayList<Refrigerator>();
	}
	public boolean removeFood(int refrigerator, String food) {
		boolean found = false;
		for(int i = 0; i< fridges.get(refrigerator).food.size(); i++) {
			if(fridges.get(refrigerator).food.get(i).equals(food)) {
				found = true;
				fridges.get(refrigerator).food.remove(i);
				break;
			}
				
		}
		return found;
	}
	public void printFridges() {
		for(int i = 0; i< fridges.size(); i++) {
			System.out.print("Fridge " + i + ": ");
			fridges.get(i).printFood();
			System.out.println();
		}
		
	}
}
