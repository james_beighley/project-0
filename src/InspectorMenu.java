package src;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class InspectorMenu {
	
	static Scanner userinput = new Scanner(System.in);
	static File loginfile = new File("src/InspectorLoginInfo.txt");

	public static void InspectorStart() {
		System.out.println("enter '1' to login, '2' to make a new account, anything else to quit");
		String choice = userinput.nextLine();
		
		if(choice.equals("1")) {
			login();
		}
		else if(choice.equals("2")) {
			signup();
		}
		else
			System.out.println();
	}

	private static void login() {
		boolean notfound = true;
		System.out.println("Please enter your username:");
		String username = userinput.next();
		userinput.nextLine();
		System.out.println("Please enter your password:");
		String password = userinput.next();
		userinput.nextLine();
		try {
			Scanner scanloginfile = new Scanner(loginfile);
			while(scanloginfile.hasNextLine()) {
				String data = scanloginfile.nextLine();
				String[] separate = data.split(":");
				if(separate[0].equals(username) && separate[1].equals(password)) {
					System.out.println("Logging in...");
					insideProgram(username, password);
					notfound = false;
					break;
				}
			}
			if(notfound)
				System.out.println("cannot find a username/password combo that matches, run program again but sign up instead");
			scanloginfile.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
	}
	
	private static void signup() {
		System.out.println("Please enter a new username(no ':' or '-'):");
		String username = userinput.next();
		userinput.nextLine();
		System.out.println("Please enter a new password:");
		String password = userinput.next();
		userinput.nextLine();
		try {
			FileWriter myWriter = new FileWriter(loginfile, true);//boolean needed to ensure append to file rather than overwrite
			myWriter.append(username +":" + password + "\n");
			System.out.println("Successfully registered.");
			System.out.println("Logging in...");
			myWriter.close();
			insideProgram(username, password);
			
		} catch (IOException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
	}
	
	private static void insideProgram(String username, String password) {
		HealthInspector myhealthinspector = new HealthInspector(username);
		ArrayList<Refrigerator> allfridges = fetchFridges();
		for(int i = 0; i< allfridges.size(); i++) {
			for(int j = 0; j< allfridges.get(i).inspectors.size(); j++) {
				if(allfridges.get(i).inspectors.get(j).equals(username)) {
					myhealthinspector.fridges.add(allfridges.remove(i));
					i--;
					break;
				}
			}
		}

		
		if(myhealthinspector.fridges.size()==0) {
			System.out.println("You were not assigned any fridges!");
		}
		boolean stay = true;
		while(stay) {
			myhealthinspector.printFridges();
			System.out.println("enter the (number) to invoke the command, enter anything else to quit");
			System.out.println("(1) remove food from fridge");
			String input = userinput.nextLine();
			switch(input) {
				case "1":
					System.out.println("Select a fridge to withdraw food from (0 to " + (myhealthinspector.fridges.size()-1) + "):");
					int fridgetowithdraw = userinput.nextInt();
					userinput.nextLine();
					myhealthinspector.fridges.get(fridgetowithdraw).printFood();
					System.out.println();
					System.out.println("Type one of the above options to withdraw");
					String foodtowithdraw = userinput.nextLine();
					Boolean withdrawnItem = myhealthinspector.removeFood(fridgetowithdraw, foodtowithdraw);
					if(withdrawnItem == true)
						System.out.println("successfully removed food");
					else 
						System.out.println("food was not found in fridge or fridge was empty");
					
					break;
				default:
					stay = false;
					for(int i = 0; i< myhealthinspector.fridges.size();i++)
						allfridges.add(myhealthinspector.fridges.get(i));
					saveChanges(allfridges);
					System.out.println("signed out");
			}
		}
	}
	
	private static void saveChanges(ArrayList<Refrigerator> allfridges) {
		try {
			FileWriter myWriter = new FileWriter("FridgeData.txt");
			for(int i = 0; i< allfridges.size(); i++) {
				StringBuilder s = new StringBuilder();
				s.append(allfridges.get(i).restaurant);
				s.append(":");
				for(int j = 0; j< allfridges.get(i).food.size(); j++) {
					s.append(allfridges.get(i).food.get(j));
					if(j != allfridges.get(i).food.size()-1)
						s.append("-");
				}
				s.append(":");
				for(int j = 0; j< allfridges.get(i).inspectors.size(); j++) {
					s.append(allfridges.get(i).inspectors.get(j));
					if(j != allfridges.get(i).inspectors.size()-1)
						s.append("-");
				}
				myWriter.append(s.toString() + "\n");
			}
			
			myWriter.close();
			
		} catch (IOException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
	}
	
	
	//loads all fridges stored in the system.
	//format:     name:food:inspectors
	
	private static ArrayList<Refrigerator> fetchFridges(){
		ArrayList<Refrigerator> fridges = new ArrayList<Refrigerator>();
		int currentFridge = 0;
		try {
			//System.out.println("inside try block");
			File userfile = new File("FridgeData.txt");
			Scanner scanuserfile = new Scanner(userfile);
			while(scanuserfile.hasNextLine()) {
				//System.out.println("scanline");
				String data = scanuserfile.nextLine();
				//System.out.println(data);
				String[] separate = data.split(":");
				String name = separate[0];
				fridges.add(new Refrigerator(name));
				if(separate.length>1) {
					String[] foodinfridge = separate[1].split("-");
					for(int j = 0; j< foodinfridge.length; j++){
						fridges.get(currentFridge).food.add(foodinfridge[j]);	
					}
				}
				if(separate.length>2) {
					String[] inspectorsforfridge = separate[2].split("-");
					
					for(int j = 0; j< inspectorsforfridge.length; j++){
						fridges.get(currentFridge).inspectors.add(inspectorsforfridge[j]);	
					}
				}
				
				currentFridge++;
			}
			scanuserfile.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("error occurred");
			e.printStackTrace();
		}
		
		return fridges;
		
		
	}
}
