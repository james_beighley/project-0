package src;

import java.util.ArrayList;

public class Refrigerator {
	String restaurant;
	ArrayList<String> food;
	ArrayList<String> inspectors;
	public Refrigerator(String restaurant){
		this.restaurant = restaurant;
		food = new ArrayList<String>();
		inspectors = new ArrayList<String>();
	}
	
	void printFood() {
		for(int i = 0; i<food.size(); i++) {
			System.out.print(i + ": " + food.get(i) + "  ");
		}
	}
	
}
